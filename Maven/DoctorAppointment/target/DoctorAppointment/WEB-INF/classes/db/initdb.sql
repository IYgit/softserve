DROP TABLE IF EXISTS appointments;
DROP TABLE IF EXISTS workdays;
-- DROP SEQUENCE IF EXISTS global_seq;

-- CREATE SEQUENCE global_seq START 1;

CREATE TABLE workdays
(
  id    SERIAL PRIMARY KEY NOT NULL,
  start TIME NOT NULL,
  "end" TIME NOT NULL
);

CREATE TABLE public.appointments
(
  id         SERIAL PRIMARY KEY NOT NULL,
  workday_id INT NOT NULL ,
  "from"     TIME NOT NULL,
  duration   INT  NOT NULL,
  CONSTRAINT appointments_workdays_id_fk
  FOREIGN KEY (workday_id) REFERENCES workdays (id) ON DELETE CASCADE
);



