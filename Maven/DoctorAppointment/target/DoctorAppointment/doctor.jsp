<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://clinic.util/functions" %>

<html>
<head>
    <title>Doctor</title>
    <style>
        .free {
            color: rgba(0, 128, 0, 0.56);
        }

        .busy {
            color: rgba(255, 0, 0, 0.46);
        }
    </style>
</head>
<body>
<H3><a href="index.jsp">Clinic</a></H3>
<br>

<table border="1" cellpadding="8" cellspacing="0">
    <thead>
    <tr>
        <th>From</th>
        <th>Duration</th>
    </tr>
    </thead>
    <c:forEach items="${timeSlots}" var="slot">
        <jsp:useBean id="slot" scope="page" type="clinic.to.TimeSlot"/>
        <tr class="${slot.free ? 'free' : 'busy'}">
            <td>${slot.from}</td>
            <td>${fn:formatDuration(slot.duration)}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
