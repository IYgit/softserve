<%--
  Created by IntelliJ IDEA.
  User: iy
  Date: 10.11.18
  Time: 20:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Appointment</title>
</head>
<body>
<H3><a href="doctor">Free time slots</a></H3>
<br>
<h3>Meals</h3>

<jsp:useBean id="slot" scope="request" type="clinic.model.Appointment"/>
<form method="post" action="patient?action=chosen">
    <dt>From Time:</dt>
    <dd><input type="time" name="from" value="${slot.from}"></dd>
    </dl>
    <dl>
        <dt>Duration:</dt>
        <dd><input type="number" value="${slot.duration.toMinutes()}" name="duration" required></dd>
    </dl>
    <button type="submit">OK</button>
</form>
</body>
</html>
