<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://clinic.util/functions" %>

<html>
<head>
    <title>Patient</title>
</head>
<body>
    <H3><a href="index.jsp">Clinic</a></H3>
    <br>

    <table border="1" cellpadding="8" cellspacing="0">
        <thead>
        <tr>
            <th>From</th>
            <th>Duration</th>
            <th></th>
        </tr>
        </thead>
        <c:forEach items="${freeSlots}" var="slot">
            <jsp:useBean id="slot" scope="page" type="clinic.model.Appointment"/>
            <tr>
                <td>${slot.from}</td>
                <td>${fn:formatDuration(slot.duration)}</td>
                <td><a href="patient?action=choose&from=${slot.from}&duration=${slot.duration}">choose</a></td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
