package clinic.to;

import clinic.model.Appointment;
import lombok.Getter;
import lombok.Setter;

import java.time.Duration;
import java.time.LocalTime;

@Getter
@Setter
public class TimeSlot implements Comparable<TimeSlot>{
    private LocalTime from;
    private Duration duration;
    private boolean free;

    public TimeSlot(LocalTime from, Duration duration, boolean free) {
        this.from = from;
        this.duration = duration;
        this.free = free;
    }

    public TimeSlot(Appointment appointment, boolean isFree){
        this(appointment.getFrom(), appointment.getDuration(), isFree);
    }

    @Override
    public int compareTo(TimeSlot o) {
        return (int) Duration.between(o.getFrom(), getFrom()).toMinutes();
    }
}
