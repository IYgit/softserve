package clinic.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class DatabaseConnector {
    Logger log = LogManager.getLogger(DatabaseConnector.class);
    private String jdbcDriver;
    private String dbUrl;
    private String user;
    private String password;

    private Connection connection;
    private Statement statement;

    {
        init();
    }

    private void init() {
        Properties prop = new Properties();
        String filename = "postgres.properties";

        try (InputStream input = this.getClass().getClassLoader().getResourceAsStream(filename)) {
            prop.load(input);

            jdbcDriver = prop.getProperty("database.driverClassName");
            dbUrl = prop.getProperty("database.url");
            user = prop.getProperty("database.username");
            password = prop.getProperty("database.password");

        } catch (IOException  e) {
            log.error(e.getMessage());
        }
    }

    //--- Connection ---
    public Connection createConnection() {
        try {
            Class.forName(jdbcDriver);
            setConnection(DriverManager.getConnection(dbUrl, user, password));

            return getConnection();
        } catch (ClassNotFoundException | SQLException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public void closeConnection() {
        try {
            if (getStatement() != null || getConnection() != null)
                getConnection().close();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }

    public Connection getConnection() {
        if (connection == null){
            setConnection(createConnection());
        }
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    //--- Statement ---
    public Statement createStatement() {
        try {
            setStatement(getConnection().createStatement());
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return getStatement();
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    //--- Query ---
    public ResultSet executeQuery(String query) {
        try {
            return getStatement().executeQuery(query);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }

        return null;
    }

    public int executeUpdate(String query) {
        try {
            return getStatement().executeUpdate(query);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }

        return 0;
    }
}
