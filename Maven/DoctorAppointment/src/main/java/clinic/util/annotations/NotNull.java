package clinic.util.annotations;

import clinic.util.advices.NotNullAdvice;
import org.aopalliance.aop.Advice;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
public @interface NotNull {
    Class<? extends Advice>[] advices() default {NotNullAdvice.class};
}
