package clinic.util.advices;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.aop.MethodBeforeAdvice;

import javax.validation.*;
import java.lang.reflect.Method;
import java.util.Set;

public class ValidateAdvice implements MethodBeforeAdvice {
    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();

        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(target);
        String message = null;
        String pattern = "Validation exception at %s  property: [%s], value: [%s], message: [%s]";
        Logger logger = LogManager.getLogger(target.getClass());

        if (constraintViolations.size() > 0){
            for (ConstraintViolation<Object> cv : constraintViolations){
                message = String.format(pattern, target.getClass().getName(), cv.getPropertyPath(), cv.getInvalidValue(), cv.getMessage());
                logger.error(message);
            }
            throw new ValidationException(message);
        }
    }
}
