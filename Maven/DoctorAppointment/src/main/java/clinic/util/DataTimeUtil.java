package clinic.util;

import java.time.Duration;

public class DataTimeUtil {

    public static String toString(Duration duration){
        return duration.toString().replace("PT", "");
    }
}
