package clinic.util;

import clinic.model.Appointment;
import clinic.service.WorkDayService;
import clinic.to.TimeSlot;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class TestUtil {
    private static AtomicInteger id = new AtomicInteger(0);
    public static WorkDayService wd = WorkDayService.getInstance();

    static {
        addAppointment(Appointment.getInstance(LocalTime.of(10, 0), Duration.of(60, ChronoUnit.MINUTES)));
        addAppointment(Appointment.getInstance(LocalTime.of(12, 0), Duration.of(60, ChronoUnit.MINUTES)));
        addAppointment(Appointment.getInstance(LocalTime.of(15, 0), Duration.of(60, ChronoUnit.MINUTES)));
    }

    public static void addAppointment(Appointment appointment){
        appointment.setId(id.incrementAndGet());
        wd.getAppointments().add(appointment);
    }

    public static List<TimeSlot> getTimeSlots(){
        final Stream<TimeSlot> busySlotsStrem = wd.getAppointments().stream().map(app -> new TimeSlot(app, false));
        final Stream<TimeSlot> freeSlotStream = wd.getFreeSlots().stream().map(app -> new TimeSlot(app, true));

        return Stream.concat(busySlotsStrem, freeSlotStream).sorted().collect(toList());
    }


}
