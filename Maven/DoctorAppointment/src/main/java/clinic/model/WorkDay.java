package clinic.model;

import clinic.util.ProxyGenerator;
import clinic.util.annotations.Validate;
import org.apache.logging.log4j.LogManager;

import javax.validation.ValidationException;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.Set;

//TODO зробити валідацію з порівнянням полів
public class WorkDay extends AbstractBaseEntity{
    @NotNull
    protected LocalTime start;
    @NotNull
    protected LocalTime end;
    @NotNull
    protected Set<Appointment> appointments;

    public static WorkDay instance(){
        return new ProxyGenerator<WorkDay>().getProxy(new WorkDay());
    }

    public Set<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(Set<Appointment> appointments) {
        this.appointments = appointments;
    }

    public LocalTime getStart() {
        return start;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "WorkDay{" +
                "start=" + start +
                ", end=" + end +
                ", appointments=" + appointments +
                '}';
    }

    @Override
    @Validate
    public void validate() {
        if (end.equals(start) || end.isBefore(start)){
            String message = String.format("end must be after start: start = %s, end = %s", start, end);
            LogManager.getLogger(this.getClass()).error(message);
            throw new ValidationException(message);
        }
    }
}
