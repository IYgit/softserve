package clinic.dao;

import clinic.model.Appointment;
import clinic.model.WorkDay;
import clinic.util.DatabaseConnector;
import clinic.util.exceptions.IllegalRequestException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

//TODO використовувати для sql запитів prepared statement
public class WorkDayDaoImpl implements WorkDayDao {
    Logger log = LogManager.getLogger(WorkDayDaoImpl.class);
    private DatabaseConnector connector = new DatabaseConnector();
    private AppointmentDaoImpl appointmentDao = new AppointmentDaoImpl();

    @Override
    public WorkDay save(WorkDay workDay) {
        connector.createStatement();
        try {
            String start = DateTimeFormatter.ofPattern("HH:mm:ss").format(workDay.getStart());
            String end = DateTimeFormatter.ofPattern("HH:mm:ss").format(workDay.getEnd());

            if (workDay.isNew()) {
                int newId = connector.executeUpdate(
                        "INSERT INTO workdays (start, \"end\") VALUES (" +
                                start + ", " + end + ")");
                workDay.setId(newId);
            } else {
                if (connector.executeUpdate("UPDATE workdays SET start = " +
                        start + ", end = " + end) == 0) {
                    log.error("Request is failed for workDay " + workDay);
                    throw new IllegalRequestException("Request is failed for workDay " + workDay);
                }
            }

            for (Appointment appointment : workDay.getAppointments()) {
                appointmentDao.save(appointment, workDay.getId());
            }

            return workDay;
        } finally {
            connector.closeConnection();
        }
    }

    @Override
    public boolean delete(int id) {
        connector.createStatement();
        try {
            return connector.executeUpdate("DELETE FROM workdays WHERE workdays.id = id") != 0;
        } finally {
            connector.closeConnection();
        }
    }

    public WorkDay get(int id) {
        connector.createStatement();
        ResultSet set = connector.executeQuery("SELECT * FROM workdays WHERE workdays.id = id");
        WorkDay workDay = new WorkDay();

        try {
            if (!set.next()){
                return null;
            }
            workDay.setId(set.getInt("id"));
            workDay.setStart(LocalTime.parse(set.getString("start"), DateTimeFormatter.ISO_LOCAL_TIME));
            workDay.setEnd(LocalTime.parse(set.getString("end"), DateTimeFormatter.ISO_LOCAL_TIME));

            return workDay;
        } catch (SQLException e) {
            log.error(e.getMessage());
        } finally {
            connector.closeConnection();
        }

        return null;
    }

    @Override
    public List<WorkDay> getAll() {
        connector.createStatement();
        ResultSet set = connector.executeQuery("SELECT * FROM workdays");
        List<WorkDay> list = new ArrayList<>();
        try {
            while (set.next()) {
                WorkDay workDay = new WorkDay();
                workDay.setId(set.getInt("id"));
                workDay.setStart(LocalTime.parse(set.getString("start"), DateTimeFormatter.ISO_LOCAL_TIME));
                workDay.setEnd(LocalTime.parse(set.getString("end"), DateTimeFormatter.ISO_LOCAL_TIME));
                list.add(workDay);
            }
            return list;
        } catch (SQLException e) {
            log.error(e.getMessage());
        } finally {
            connector.closeConnection();
        }
        return null;
    }

    public List<WorkDay> getAllWithAppointments() {
        connector.createStatement();
        ResultSet set = connector.executeQuery("SELECT * from workdays as wd\n" +
                "LEFT JOIN appointments as ap on wd.id = ap.workday_id\n" +
                "WHERE ap.id is NOT NULL\n" +
                "ORDER BY wd.id, ap.workday_id");
        Map<Integer, WorkDay> dayMap = new HashMap<>();

        try {
            while (set.next()) {
                Integer id = set.getInt("workday_id");
                Appointment appointment = Appointment.getInstance();
                appointment.setId(id);
                appointment.setFrom(LocalTime.parse(set.getString("from"), DateTimeFormatter.ISO_LOCAL_TIME));
                appointment.setDuration(Duration.of(set.getInt("duration"), ChronoUnit.MINUTES));

                WorkDay workDay = dayMap.get(id);
                if (workDay == null){
                    workDay = WorkDay.instance();
                    workDay.setId(id);
                    workDay.setStart(LocalTime.parse(set.getString("start"), DateTimeFormatter.ISO_LOCAL_TIME));
                    workDay.setEnd(LocalTime.parse(set.getString("end"), DateTimeFormatter.ISO_LOCAL_TIME));
                }
                workDay.getAppointments().add(appointment);
            }

            return new ArrayList<>(dayMap.values());
        } catch (SQLException e) {
            log.error(e.getMessage());
        } finally {
            connector.closeConnection();
        }
        return null;
    }
}
