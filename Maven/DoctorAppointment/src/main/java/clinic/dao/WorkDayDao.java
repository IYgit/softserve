package clinic.dao;

import clinic.model.WorkDay;

import java.util.List;

public interface WorkDayDao {
    WorkDay save(WorkDay workDay);

    boolean delete(int id);

    WorkDay get(int id);

    List<WorkDay> getAll();

    List<WorkDay> getAllWithAppointments();
}
