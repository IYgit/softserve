package clinic.dao;

import clinic.model.Appointment;

import java.time.LocalTime;
import java.util.List;

public interface AppointmentDao {
    Appointment save(Appointment appointment, int workDayId);

    boolean delete(int id);

    Appointment get(int id);

    List<Appointment> getAll(int workDayId);
}
