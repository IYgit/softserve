package clinic.web;

import clinic.model.Appointment;
import clinic.util.TestUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;

public class PatientServlet extends HttpServlet {
    Logger log = LogManager.getLogger(PatientServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        try {
            switch (action == null ? "display" : action) {
                case "choose":
                    request.setAttribute("slot", createAppointment(request));
                    request.getRequestDispatcher("/appointmentForm.jsp").forward(request, response);
                    break;
                case "display":
                    request.setAttribute("freeSlots", TestUtil.wd.getFreeSlots());
                    break;
            }
        } catch (ServletException | IOException e) {
            log.error(e.getMessage());
        }

        request.getRequestDispatcher("/patient.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            TestUtil.addAppointment(createAppointment(request));
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        response.sendRedirect("patient");
    }

    private Appointment createAppointment(HttpServletRequest request) {
        LocalTime from = LocalTime.parse(request.getParameter("from"));
        Duration duration = null;
        switch (request.getParameter("action")){
            case "choose":
                duration = Duration.parse(request.getParameter("duration"));
                break;
            case "chosen":
                duration = Duration.ofMinutes(Long.parseLong(request.getParameter("duration")));
                break;
        }
        Appointment appointment = Appointment.getInstance(from, duration);
        appointment.validate();

        return appointment;
    }
}
