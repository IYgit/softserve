package clinic.web;

import clinic.model.Appointment;
import clinic.util.TestUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

public class DoctorServlet extends HttpServlet{
    Logger log = LogManager.getLogger(DoctorServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("redirect to doctor.jsp");
        request.setAttribute("timeSlots", TestUtil.getTimeSlots());

        request.getRequestDispatcher("/doctor.jsp").forward(request, response);
    }
}
