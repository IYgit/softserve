package clinic.service;

import clinic.model.Appointment;
import clinic.model.WorkDay;
import clinic.util.ProxyGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class WorkDayService extends WorkDay {
    Logger log = LogManager.getLogger(WorkDayService.class);

    public WorkDayService() {
    }

    public static WorkDayService getInstance(){
        return new ProxyGenerator<WorkDayService>().getProxy(new WorkDayService());
    }

    {
        Properties prop = new Properties();
        String filename = "clinic.properties";

        try (InputStream input = this.getClass().getClassLoader().getResourceAsStream(filename)) {
            prop.load(input);
//            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("hh:mm:ss");
            start = LocalTime.parse(prop.getProperty("timeStart"), DateTimeFormatter.ISO_LOCAL_TIME);
            end = LocalTime.parse(prop.getProperty("timeEnd"), DateTimeFormatter.ISO_LOCAL_TIME);
            appointments = new TreeSet<>();

        } catch (IOException e) {
            log.error(e.getMessage());
        }
        validate();
    }

    /**
     * Adding new appointment
     *
     * @param appointment - new appointment;
     * @return - true, if appointment is successful added, otherwise - false;
     */
    public boolean addAppointment(Appointment appointment) {
        appointment.validate();
        log.info("addAppointment {}", appointment);
        Set<Appointment> nearestSlots = getNearestSlots(appointment);
        if (checkAppointment(nearestSlots, appointment)) {
            appointments.add(appointment);
            return true;
        }
        return false;
    }

    /**
     * Print nearest slots.
     *
     * @param slots - set of nearest free slots;
     */
    public void printSlots(Set<Appointment> slots) {
        for (Appointment appointment : slots) {
            System.out.println(appointment);
        }
    }

    /**
     * Check whether the appointment is suitable to free slots;
     *
     * @param nearestSlots - sorted nearest free slots;
     * @param appointment  - client appointment;
     * @return - true, if appointment is suitable, otherwise - false;
     */
    public boolean checkAppointment(Set<Appointment> nearestSlots, Appointment appointment) {
        log.info("checkAppointment {}, {}", nearestSlots, appointment);
        Appointment firstSlot = nearestSlots.iterator().next();
        LocalTime to = firstSlot.getFrom().plusMinutes(firstSlot.getDuration().toMinutes());

        return isBetween(appointment, firstSlot.getFrom(), to);
    }

    /**
     * Check whether the client appointment is in range from slotFrom including and
     * slotTo excluding;
     *
     * @param appointment - client desired appointment;
     * @param slotFrom    - start of time slot;
     * @param slotTo      - end of time slot;
     * @return - true, if appointment is in range, otherwise - false;
     */
    public boolean isBetween(Appointment appointment, LocalTime slotFrom, LocalTime slotTo) {
        log.info("isBetween {}, {}, {}", appointment, slotFrom, slotTo);
        LocalTime to = appointment.getFrom().plusMinutes(appointment.getDuration().toMinutes());

        return (appointment.getFrom().equals(slotFrom) || appointment.getFrom().isAfter(slotFrom)) && to.isBefore(slotTo);
    }

    /**
     * Getting nearest to client appointment sorted free slots.
     *
     * @param slots       - set of free slots;
     * @param appointment - client desired appointment;
     * @return - set of nearest free slots;
     */
    public Set<Appointment> getNearestSlots(Set<Appointment> slots, Appointment appointment) {
        log.info("getNearestSlots {}, {}", slots, appointment);
        Comparator<Appointment> comparator = Comparator.comparingLong(ap -> Math.abs(Duration.between(ap.getFrom(), appointment.getFrom()).toMinutes()));
        comparator = comparator.thenComparing(Comparator.comparing(Appointment::getFrom));

        Set<Appointment> nearestSet = new TreeSet<>(comparator);
        nearestSet.addAll(slots);

        return nearestSet;
    }

    /**
     * Getting nearest to client appointment sorted free slots.
     *
     * @param appointment - client desired appointment;
     * @return - set of nearest free slots;
     */
    public Set<Appointment> getNearestSlots(Appointment appointment) {
        log.info("getNearestSlots {}", appointment);
        return getNearestSlots(getFreeSlots(), appointment);
    }

    /**
     * Getting free slots.
     *
     * @return - set of free slots;
     */
    public Set<Appointment> getFreeSlots() {
        log.info("getFreeSlots ");
        Set<Appointment> freeSlots = new TreeSet<>();
        if (appointments.isEmpty()) {
            freeSlots.add(Appointment.getInstance(start, Duration.between(start, end)));
            return freeSlots;
        }

        Iterator<Appointment> iterator = appointments.iterator();
        LocalTime freeSlotFrom = start;
        for (Appointment app; iterator.hasNext(); ) {
            app = iterator.next();
            Duration duration = Duration.between(freeSlotFrom, app.getFrom());
            if (duration.toMinutes() > 0) {
                freeSlots.add(Appointment.getInstance(freeSlotFrom, duration));
            }
            freeSlotFrom = app.getFrom().plus(app.getDuration());
        }

        Duration duration = Duration.between(freeSlotFrom, end);
        if (duration.toMinutes() > 0) {
            freeSlots.add(Appointment.getInstance(freeSlotFrom, duration));
        }

        return freeSlots;
    }
}
