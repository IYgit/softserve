package clinic.service;

import clinic.model.Appointment;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.validation.ValidationException;
import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import static org.testng.Assert.*;

/* Проблеми та рішення
1. проект використовує неправильну версію JDK (usage of API Documented as @scince 1.8+ more)
рішення:
    1.1 File -> Project Structure -> Project Settings -> Modules -> "назва_модуля" -> Sources -> Language Level: вибрати рівень JDK
    1.2 File -> Settings... -> Java Compiler -> Target bytecode version: встановити версію

*/
public class WorkDayServiceTest {
    private static WorkDayService wd;
    private final Appointment clientApp = Appointment.getInstance(LocalTime.of(11, 0), Duration.of(30, ChronoUnit.MINUTES));
    private final Appointment clientApp_2 = Appointment.getInstance(LocalTime.of(12, 0), Duration.of(30, ChronoUnit.MINUTES));

    @BeforeMethod
    public static void beforeMethod(){
//        wd = new WorkDayService();
        wd = WorkDayService.getInstance();
        Set<Appointment> appointmentSet = wd.getAppointments();
        appointmentSet.add(Appointment.getInstance(LocalTime.of(10, 0), Duration.of(60, ChronoUnit.MINUTES)));
        appointmentSet.add(Appointment.getInstance(LocalTime.of(12, 0), Duration.of(60, ChronoUnit.MINUTES)));
        appointmentSet.add(Appointment.getInstance(LocalTime.of(15, 0), Duration.of(60, ChronoUnit.MINUTES)));
    }

    @Test
    public void successfulAddAppointmentTest() {
        assertTrue(wd.addAppointment(clientApp));
    }

    @Test
    public void failAddAppointmentTest() {
        assertFalse(wd.addAppointment(clientApp_2));
    }

    @Test(expectedExceptions = ValidationException.class)
    public void workDayValidationTest(){
        wd.setEnd(null);
        wd.validate();
    }

    @Test(expectedExceptions = ValidationException.class)
    public void appointmentValidationTest(){
        Appointment appointment = Appointment.getInstance();
        appointment.validate();
    }



    @Test
    public void getFreeSlotsTest() {
        Set<Appointment> expectedSet = new TreeSet<>();
        expectedSet.add(Appointment.getInstance(LocalTime.of(8, 0), Duration.of(2, ChronoUnit.HOURS)));
        expectedSet.add(Appointment.getInstance(LocalTime.of(11, 0), Duration.of(1, ChronoUnit.HOURS)));
        expectedSet.add(Appointment.getInstance(LocalTime.of(13, 0), Duration.of(2, ChronoUnit.HOURS)));
        expectedSet.add(Appointment.getInstance(LocalTime.of(16, 0), Duration.of(1, ChronoUnit.HOURS)));

        assertEquals(wd.getFreeSlots(), expectedSet);
    }

    @Test
    public void getNearestSlotsTest() {
        Set<Appointment> expectedSet = new TreeSet<>(Comparator.comparingLong(
                ap -> Math.abs(Duration.between(ap.getFrom(), clientApp.getFrom()).toMinutes())));
        expectedSet.add(Appointment.getInstance(LocalTime.of(11, 0), Duration.of(1, ChronoUnit.HOURS)));
        expectedSet.add(Appointment.getInstance(LocalTime.of(8, 0), Duration.of(2, ChronoUnit.HOURS)));
        expectedSet.add(Appointment.getInstance(LocalTime.of(13, 0), Duration.of(2, ChronoUnit.HOURS)));
        expectedSet.add(Appointment.getInstance(LocalTime.of(16, 0), Duration.of(1, ChronoUnit.HOURS)));

        assertEquals(wd.getNearestSlots(clientApp), expectedSet);
    }

    @Test
    public void isAppointmentInTimeBoundariesTest() {
        assertTrue(wd.isBetween(clientApp, LocalTime.of(11, 0), LocalTime.of(12, 0)));
        assertTrue(wd.isBetween(clientApp, LocalTime.of(11, 0), LocalTime.of(11, 31)));
    }

    @Test
    public void isNotAppointmentInTimeBoundariesTest() {
        assertFalse(wd.isBetween(clientApp, LocalTime.of(11, 0), LocalTime.of(11, 30)));
        assertFalse(wd.isBetween(clientApp, LocalTime.of(11, 1), LocalTime.of(12, 0)));
    }

    @Test
    public void isAppointmentInNearestFreeSlot() {
        assertTrue(wd.checkAppointment(wd.getNearestSlots(wd.getFreeSlots(), clientApp), clientApp));
    }

    @Test
    public void isNotAppointmentInNearestFreeSlot() {
        assertFalse(wd.checkAppointment(wd.getNearestSlots(wd.getFreeSlots(), clientApp_2), clientApp_2));
    }

}