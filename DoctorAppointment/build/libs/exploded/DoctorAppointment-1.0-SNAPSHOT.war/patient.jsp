<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://clinic.util/functions" %>

<html>
<head>
    <title>Patient</title>
    <style>
        .free {
            color: rgba(0, 0, 0, 0.76);
        }

        .busy {
            color: rgb(49, 253, 8);
        }
    </style>
</head>
<body>
<H3><a href="index.jsp">Home</a></H3>
<br>

<table border="1" cellpadding="8" cellspacing="0">
    <thead>
    <tr>
        <th>From</th>
        <th>To</th>
        <th></th>
    </tr>
    </thead>
    <c:forEach items="${slots}" var="slot">
        <jsp:useBean id="slot" scope="page" type="clinic.to.TimeSlot"/>
        <tr class="${slot.free ? 'free' : 'busy'}">
            <td>${slot.from}</td>
            <td>${fn:durationToTimeString(slot.from, slot.duration)}</td>
            <td><a href="patient?action=${slot.free ? 'choose' : 'delete'}&from=${slot.from}&duration=${slot.duration}">${slot.free ? 'choose' : 'delete'}</a></td>
        </tr>
    </c:forEach>
</table>
<br>
<button type="submit"><a href="patient?action=save">Save</a></button>
<button type="button"><a href="patient?action=cancel">Logout</a></button>
</body>
</html>
