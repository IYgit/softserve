DELETE FROM appointments;
DELETE FROM workdays;

INSERT INTO workdays (start, "end") VALUES
  ('08:00', '17:00'),
  ('08:00', '17:00');

INSERT INTO appointments (workday_id, "from", duration) VALUES
  (1,'10:00', 60),
  (1,'12:00', 60),
  (1,'15:00', 60);