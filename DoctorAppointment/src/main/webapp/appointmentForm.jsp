<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fn" uri="http://clinic.util/functions" %>


<html>
<head>
    <title>Appointment</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<H3><a href="doctor">Free time slots</a></H3>
<br>
<%--<jsp:useBean id="slot" scope="request" type="clinic.model.Appointment"/>--%>
<jsp:useBean id="slot" scope="request" type="clinic.to.TimeSlot"/>


<form method="post" onsubmit="return timeValidation()" action="patient?action=chosen">
    From time:
    <input id="fromTime" type="time" name="from" value=${slot.from}>
    <br>
    <br>
    To time:
    <input id="toTime" type="time" name="to" value=${fn:durationToTime(slot.from, slot.duration)}>
    <br>
    <br>
    <button id="check" type="submit">OK</button>
</form>
<div id="appFrom" hidden="true">${slot.from}</div>
<div id="appTo" hidden="true">${fn:durationToTime(slot.from, slot.duration)}</div>

<script defer>

    function timeValidation() {
        var appFrom = $('#appFrom').html();
        var appTo = $('#appTo').html();
        var timeFrom = document.querySelector('#fromTime').value;
        var timeTo = document.querySelector('#toTime').value;

        if (timeFrom < appFrom){
            alert("From time can't be less then " + appFrom);
            return false;
        } else if (timeTo > appTo){
            alert("To time can't be more then " + appTo);
            return false;
        } else if (timeTo <= timeFrom){
            alert("From time can't be more then " + timeTo + " or equals to it!");
            return false;
        }
        return true;
    }
</script>
</body>
</html>
