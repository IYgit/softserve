package clinic.web;

import clinic.model.Appointment;
import clinic.model.WorkDay;
import clinic.to.TimeSlot;
import clinic.util.ServletTestUtil;
import clinic.web.appointment.AppointmentController;
import clinic.web.appointment.AppointmentRestController;
import clinic.web.workday.WorkDayRestController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Time;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
// для демо:
//TODO: розгорнути war в tomcat (без idea)
//TODO: створити дамп БД

//TODO: зробити update (edit) для новоствореного слота
//TODO: зробити інтернаціоналізацію
//TODO: винести допоміжні методи в ServletUtil
//TODO: зробити повідомлення (Notification framework)
//TODO: підключити DataTables framework
//TODO: validation + logging
//TODO: зробити сторінку авторизаці: по логіну і паролю визначаємо роль і направляємо на відповідну сторінку
public class PatientServlet extends HttpServlet {
    private final String NEW_APPOINTMENTS = "newAppointments";
    private Logger log = LogManager.getLogger(PatientServlet.class);
    private AppointmentController controller = new AppointmentRestController();
    private static final WorkDay WORK_DAY = new WorkDayRestController().get(1);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");

        try {
            switch (action == null ? "display" : action) {
                case "choose":
                    request.setAttribute("slot", createTimeSlot(request, false));
                    request.getRequestDispatcher("/appointmentForm.jsp").forward(request, response);
                    break;
                case "display":
                    request.setAttribute("slots", getPatientAndFreeSlots(request.getSession()));
                    request.getRequestDispatcher("/patient.jsp").forward(request, response);
                    break;
                case "cancel":
                    request.getSession().invalidate();
                    request.getRequestDispatcher("index.jsp").forward(request, response);
                    break;
                case "save":
                    for (TimeSlot timeSlot : (List<TimeSlot>) request.getSession().getAttribute(NEW_APPOINTMENTS)) {
                        controller.create(timeSlot, WORK_DAY.getId());
                        request.setAttribute("slots", controller.getFreeSlots(WORK_DAY));
                    }
                    request.getSession().setAttribute(NEW_APPOINTMENTS, null);
                    request.getRequestDispatcher("/patient.jsp").forward(request, response);
                    break;
                case "delete":
                    deleteSlotFromSession(request);
                    request.setAttribute("slots", getPatientAndFreeSlots(request.getSession()));
                    request.getRequestDispatcher("/patient.jsp").forward(request, response);
            }
        } catch (ServletException | IOException e) {
            log.error(e.getMessage());
        }
        request.getRequestDispatcher("/patient.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            addAppointmentToSession(request.getSession(), createTimeSlot(request, false));
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        response.sendRedirect("patient");
    }

    private void deleteSlotFromSession(HttpServletRequest request) {
        List<TimeSlot> timeSlots = new ArrayList<>((List<TimeSlot>) request.getSession().getAttribute(NEW_APPOINTMENTS));
        LocalTime from = LocalTime.parse(request.getParameter("from"));

        for (TimeSlot slot: timeSlots){
            if (slot.getFrom().equals(from)){
                if (timeSlots.remove(slot)){
                    request.getSession().setAttribute(NEW_APPOINTMENTS, timeSlots);
                    return;
                } else {
                    Duration duration = Duration.parse(request.getParameter("duration"));
                    String message = String.format("Removing of slot with parameters from = %s, duration = %s is failed", from, duration);
                    log.error(message);
                }
            }
        }
    }

    private List<TimeSlot> getPatientAndFreeSlots(HttpSession session) {
        List<TimeSlot> newAppointments = (List<TimeSlot>)session.getAttribute(NEW_APPOINTMENTS);
        List<TimeSlot> total = new ArrayList<>(controller.getFreeSlots(WORK_DAY));
        if (newAppointments == null){
            return total;
        } else {
            total.addAll(newAppointments);
            Comparator<TimeSlot> comparator = Comparator.comparing(TimeSlot::getFrom)
                    .thenComparing(TimeSlot::getDuration, Comparator.reverseOrder());
            total.sort(comparator);
            if (total.size() > 1){
                List<TimeSlot> removeList = new ArrayList<>();
                List<TimeSlot> addList = new ArrayList<>();
                for (int i = 1; i < total.size(); i++){
                    if (!total.get(i).isFree()){
                        if (!total.get(i).getFrom().equals(total.get(i - 1).getFrom())){
                            Appointment freeAppointment = Appointment.getInstance(total.get(i - 1).getFrom(), Duration.between(total.get(i - 1).getFrom(), total.get(i).getFrom()));
                            addList.add(freeAppointment.toTimeSlot(true));
                        }
                        if (!total.get(i).getDuration().equals(total.get(i - 1).getDuration())){
                            LocalTime from = total.get(i).getFrom().plus(total.get(i).getDuration());
                            LocalTime to = total.get(i - 1).getFrom().plus(total.get(i - 1).getDuration());
                            Appointment freeAppointment = Appointment.getInstance(from, Duration.between(from, to));
                            addList.add(freeAppointment.toTimeSlot(true));
                        }
                        removeList.add(total.get(i - 1));
                    }
                }
                total.removeAll(removeList);
                total.addAll(addList);
            }
        }
        total.sort(Comparator.comparing(TimeSlot::getFrom));
        return total;
    }

    private void addAppointmentToSession(HttpSession session, TimeSlot timeSlot) {
        try {
            List<TimeSlot> newAppointments = (List) session.getAttribute(NEW_APPOINTMENTS);
            ArrayList<TimeSlot> list = newAppointments == null ? new ArrayList<>() :
                    new ArrayList<>(newAppointments);
            list.add(timeSlot);
            session.setAttribute(NEW_APPOINTMENTS, list);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    private TimeSlot createTimeSlot(HttpServletRequest request, boolean isFree) {
        LocalTime from = LocalTime.parse(request.getParameter("from"));
        Duration duration = null;
        switch (request.getParameter("action")) {
            case "choose":
                duration = Duration.parse(request.getParameter("duration"));
                break;
            case "chosen":
                LocalTime to = LocalTime.parse(request.getParameter("to"));
                duration = Duration.between(from, to);
                break;
        }
        Appointment appointment = Appointment.getInstance(from, duration);
        appointment.validate();

        return new TimeSlot(appointment, isFree);
    }
}
