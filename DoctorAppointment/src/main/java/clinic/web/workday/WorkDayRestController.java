package clinic.web.workday;

import clinic.model.WorkDay;
import clinic.service.WorkDayService;
import clinic.service.WorkDayServiceImpl;
import clinic.util.exceptions.NotFoundException;

import java.util.List;

public class WorkDayRestController implements WorkDayController {
    private WorkDayService workDayService = new WorkDayServiceImpl();

    @Override
    public List<WorkDay> getAll() {
        return workDayService.getAll();
    }

    @Override
    public WorkDay get(int workDayId){
        return workDayService.get(workDayId);
    }

    @Override
    public WorkDay create(WorkDay workDay) {
        return workDayService.create(workDay);
    }

    @Override
    public void delete(int workDay){
        workDayService.delete(workDay);
    }

    @Override
    public void update(WorkDay workDay) {
        workDayService.update(workDay);
    }
}
