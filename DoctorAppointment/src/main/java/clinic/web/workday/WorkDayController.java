package clinic.web.workday;

import clinic.model.WorkDay;
import clinic.util.exceptions.NotFoundException;

import java.util.List;

public interface WorkDayController {

    List<WorkDay> getAll();

    WorkDay get(int workDayId);

    WorkDay create(WorkDay workDay);

    void delete(int workDay);

    void update(WorkDay workDay);
}
