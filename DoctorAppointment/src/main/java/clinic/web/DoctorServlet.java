package clinic.web;

import clinic.util.ServletTestUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DoctorServlet extends HttpServlet{
    Logger log = LogManager.getLogger(DoctorServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("timeSlots", ServletTestUtil.getBusyAndFreeTimeSlots());

        request.getRequestDispatcher("/doctor.jsp").forward(request, response);
    }
}
