package clinic.web.appointment;

import clinic.model.Appointment;
import clinic.model.WorkDay;
import clinic.to.TimeSlot;

import java.util.List;

public interface AppointmentController {

    List<TimeSlot> getFreeSlots(WorkDay workDay);

    List<TimeSlot> getBusySlots(WorkDay workDay);

    TimeSlot get(int timeSlotId);

    TimeSlot create(TimeSlot timeSlot, int workDayId);

    void delete(int timeSlotId);

    void update(TimeSlot timeSlot);
}
