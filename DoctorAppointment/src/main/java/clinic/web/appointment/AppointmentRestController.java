package clinic.web.appointment;

import clinic.model.WorkDay;
import clinic.service.AppointmentService;
import clinic.service.AppointmentServiceImpl;
import clinic.to.TimeSlot;

import java.util.List;
import java.util.stream.Collectors;

public class AppointmentRestController implements AppointmentController {
    private AppointmentService appointmentService = new AppointmentServiceImpl();

    @Override
    public List<TimeSlot> getFreeSlots(WorkDay workDay) {
        return appointmentService.getFreeSlots(workDay).stream()
                .map(ap -> ap.toTimeSlot(true)).collect(Collectors.toList());
    }

    @Override
    public List<TimeSlot> getBusySlots(WorkDay workDay) {
        return appointmentService.getAll(workDay).stream()
                .map(ap -> ap.toTimeSlot(false)).collect(Collectors.toList());
    }

    @Override
    public TimeSlot get(int timeSlotId) {
        return appointmentService.get(timeSlotId).toTimeSlot(false);
    }

    @Override
    public TimeSlot create(TimeSlot timeSlot, int workDayId) {
        return appointmentService.create(timeSlot.toAppointment(), workDayId).toTimeSlot(false);
    }

    @Override
    public void delete(int workDayId) {
        appointmentService.delete(workDayId);
    }

    @Override
    public void update(TimeSlot timeSlot) {
        appointmentService.update(timeSlot.toAppointment());
    }
}
