package clinic;

import clinic.model.Appointment;
import clinic.service.WorkDayConsoleService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
/** user=doctor; password=doctor; */
class Clinic {

    public static void main(String[] args) {
        WorkDayConsoleService workDay = WorkDayConsoleService.getInstance();
        LocalTime startTime;
        Duration duration;

        try (BufferedReader bf = new BufferedReader(new InputStreamReader(System.in))){
            while (true) {
                while (true){
                    System.out.println("Enter desired start time in format hh:mm");
                    String time = bf.readLine();
                    try {
                        startTime = LocalTime.parse(time + ":00");
                        break;
                    } catch (Exception e) {
                        if (time.equals("exit")){
                            System.exit(0);
                        }
                        System.out.println("Incorrect time");
                    }
                }

                while (true){
                    System.out.println("Enter desired duration in minutes");
                    String strDuration = bf.readLine();
                    try {
                        int amount = Integer.parseInt(strDuration);
                        if (amount <= 0){
                            throw new NumberFormatException();
                        }
                        duration = Duration.of(amount, ChronoUnit.MINUTES);
                        break;
                    } catch (NumberFormatException e) {
                        if (strDuration.equals("exit")){
                            System.exit(0);
                        }
                        System.out.println("Incorrect duration");
                    }
                }

                Appointment appointment = Appointment.getInstance(startTime, duration);
                if (workDay.addAppointment(appointment)) {
                    System.out.println("Slot is successful added.");
                } else {
                    System.out.println("Slot is busy. Choose available slot: ");
                    workDay.printSlots(workDay.getNearestSlots(appointment));
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
