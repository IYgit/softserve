package clinic.util;

import lombok.Getter;
import lombok.Setter;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Advisor;
import org.springframework.aop.ClassFilter;
import org.springframework.aop.Pointcut;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.StaticMethodMatcherPointcut;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

//TODO: springMethodReplacer -> getClass()
@Getter
@Setter
public class ProxyGenerator<T> {
    private List<Advisor> advisors;             // advosor - це пара advice (декоратор) + filter
    private T target;                           // цільовий об'єкт для створення проксі
    private Predicate<Class> testClass;         // фільтр класів (має значення за замовчанням)
    private Predicate<Method> testMethod;       // фільтр методів
    private Advice advice;                      // декоратор (об'єкт з додатковим кодом)

    public ProxyGenerator() {
        this.advisors = new ArrayList<>();
        this.testClass = p -> p.equals(target.getClass());
        this.testMethod = m -> true;
    }

    private StaticMethodMatcherPointcut getStaticMethodPointcut(Predicate<Method> predicate) {
        return new StaticMethodMatcherPointcut() {
            @Override //фльтр методів
            public boolean matches(Method method, Class<?> aClass) {
                return predicate.test(method);
            }

            @Override // фільтр класів
            public ClassFilter getClassFilter() {
                return aClass -> testClass.test(aClass);
            }
        };
    }

    private StaticMethodMatcherPointcut getStaticMethodPointcut() {
        return getStaticMethodPointcut(testMethod);
    }

    public ProxyGenerator<T> build(Pointcut pointcut, Advice advice) {
        advisors.add(new DefaultPointcutAdvisor(pointcut, advice));
        return this;
    }

    public T getProxy() {
        // проксі-фабрика
        ProxyFactory pf = new ProxyFactory();
        pf.setTarget(target);
        pf.addAdvisors(advisors);
        pf.setOptimize(true);

        return (T) pf.getProxy();
    }

    /**
     * Метод створює проксі по advice'ам, вказаних в параметрі advices анотації
     *
     * @param target - цільовий об'єкт, для якого створюється проксі;
     * @return - проксі об'єкт;
     * У класа декоратора (Advice) повинен бути корструктор без параметрів.
     * 1. у цільового об'єкта скануються всі анотації;
     * 2. якщо у анотації є параметр advie(), в якому задано за замовчанням клас декоратора,
     * то в метод анотації вставляється код декоратора;
     * 3. якщо параметра adviec() не існує, або відсутнє значення за замовчанням, анотація ігнорується.
     */
    public T getProxy(T target) {
        this.target = target;
        for (Method method : target.getClass().getMethods()) {
            Annotation[] annotations = method.getDeclaredAnnotations();
            for (Annotation ann : annotations) {
                Class<? extends Annotation> annClass = ann.annotationType();
                try {
                    Method annMethod = annClass.getMethod("advices");
                    if (annMethod == null) {
                        continue;
                    }
                    Class[] adviceClasses = (Class[]) annMethod.invoke(ann);

                    for (Class adviceClass : adviceClasses) {
                        this.advice = (Advice) adviceClass.newInstance();// об'єкт декоратора (повинен бути конструктор за замовчанням)

                        Predicate<Method> predicate = m -> m.getAnnotation(annClass) != null &&
                                m.getName().equals(method.getName()) &&
                                Arrays.equals(m.getParameterTypes(), method.getParameterTypes());
                        StaticMethodMatcherPointcut pointcut = getStaticMethodPointcut(predicate);
                        build(pointcut, advice);
                    }
                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
                    continue;
                }
            }
        }

        return getProxy();
    }
}
