package clinic.util.advices;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.NullValueInNestedPathException;

import java.lang.reflect.Method;

public class ReturnNotNullAdvice implements MethodInterceptor{

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Object retVal = invocation.proceed();
        if (retVal == null){
            Method method = invocation.getMethod();
            Object target = invocation.getThis();

            String message = String.format("%s: Return type can not be NULL in method %s", target.getClass(), method.getName());
            LogManager.getLogger(target.getClass()).error(message);
            throw new NullPointerException(message);
        }
        return retVal;
    }
}
