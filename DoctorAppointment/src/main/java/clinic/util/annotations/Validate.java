package clinic.util.annotations;

import clinic.util.advices.ValidateAdvice;
import org.aopalliance.aop.Advice;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
public @interface Validate {
    Class<? extends Advice>[] advices() default {ValidateAdvice.class};
}
