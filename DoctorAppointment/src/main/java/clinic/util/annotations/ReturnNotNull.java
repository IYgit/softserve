package clinic.util.annotations;

import clinic.util.advices.ReturnNotNullAdvice;
import org.aopalliance.aop.Advice;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
public @interface ReturnNotNull {
    Class<? extends Advice>[] advices() default {ReturnNotNullAdvice.class};
}
