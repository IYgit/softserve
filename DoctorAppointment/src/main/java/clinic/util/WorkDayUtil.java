package clinic.util;

import clinic.model.Appointment;
import clinic.model.WorkDay;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class WorkDayUtil {

    /**
     * Getting free slots.
     * @return - set of free slots;
     */
    public static Set<Appointment> getFreeSlots(WorkDay workDay) {
        Set<Appointment> freeSlots = new TreeSet<>();
        if (workDay.getAppointments().isEmpty()) {
            freeSlots.add(Appointment.getInstance(workDay.getStart(), Duration.between(workDay.getStart(), workDay.getEnd())));
            return freeSlots;
        }

        Iterator<Appointment> iterator = workDay.getAppointments().iterator();
        LocalTime freeSlotFrom = workDay.getStart();
        for (Appointment app; iterator.hasNext(); ) {
            app = iterator.next();
            Duration duration = Duration.between(freeSlotFrom, app.getFrom());
            if (duration.toMinutes() > 0) {
                freeSlots.add(Appointment.getInstance(freeSlotFrom, duration));
            }
            freeSlotFrom = app.getFrom().plus(app.getDuration());
        }

        Duration duration = Duration.between(freeSlotFrom, workDay.getEnd());
        if (duration.toMinutes() > 0) {
            freeSlots.add(Appointment.getInstance(freeSlotFrom, duration));
        }

        return freeSlots;
    }

    /**
     * Getting nearest to client appointment sorted free slots.
     *
     * @param slots       - set of free slots;
     * @param appointment - client desired appointment;
     * @return - set of nearest free slots;
     */
    public static Set<Appointment> getNearestSlots(Set<Appointment> slots, Appointment appointment) {
        Comparator<Appointment> comparator = Comparator.comparingLong(ap -> Math.abs(Duration.between(ap.getFrom(), appointment.getFrom()).toMinutes()));
        comparator = comparator.thenComparing(Comparator.comparing(Appointment::getFrom));

        Set<Appointment> nearestSet = new TreeSet<>(comparator);
        nearestSet.addAll(slots);

        return nearestSet;
    }
}
