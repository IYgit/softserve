package clinic.util;

import clinic.model.Appointment;
import clinic.service.WorkDayConsoleService;
import clinic.to.TimeSlot;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class ServletTestUtil {
    private static AtomicInteger id = new AtomicInteger(0);
    public static WorkDayConsoleService wd = WorkDayConsoleService.getInstance();

    static {
        addAppointment(Appointment.getInstance(LocalTime.of(10, 0), Duration.of(60, ChronoUnit.MINUTES)));
        addAppointment(Appointment.getInstance(LocalTime.of(12, 0), Duration.of(60, ChronoUnit.MINUTES)));
        addAppointment(Appointment.getInstance(LocalTime.of(15, 0), Duration.of(60, ChronoUnit.MINUTES)));
    }

    public static void addAppointment(Appointment appointment){
        appointment.setId(id.incrementAndGet());
        wd.getAppointments().add(appointment);
    }

    public static List<TimeSlot> getBusyAndFreeTimeSlots(){
        final Stream<TimeSlot> busySlotsStream = wd.getAppointments().stream().map(app -> new TimeSlot(app, false));
        final Stream<TimeSlot> freeSlotStream = wd.getFreeSlots().stream().map(app -> new TimeSlot(app, true));

        return Stream.concat(busySlotsStream, freeSlotStream).sorted().collect(toList());
    }

    public static List<TimeSlot> getFreeTimeSlots(){
        return getBusyAndFreeTimeSlots().stream().filter(slot -> slot.isFree()).sorted().collect(toList());
    }


}
