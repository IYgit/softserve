package clinic.util;

import clinic.model.Appointment;

import java.time.Duration;
import java.time.LocalTime;

public class DataTimeUtil {

    public static String toString(Duration duration){
        return duration.toString().replace("PT", "");
    }

    public static String toString(LocalTime localTime, Duration duration){
        return localTime.plus(duration).toString();
    }

    public static LocalTime toLocalTimeTo(LocalTime localTime, Duration duration){
        return localTime.plus(duration);
    }

    public static boolean isTimeValid(String branFrom, String beanDuration, String from, String to){
        System.out.println();
        return true;
    }

    public static boolean isTimeValid(Appointment appointment, String from, String to){
        System.out.println();
        return true;
    }

    public static LocalTime getTo(Appointment appointment){
        return appointment.getFrom().plus(appointment.getDuration());
    }

    public static boolean isTimeValid(LocalTime branFrom){
        System.out.println();

        return true;
    }

    public static String getString(){
        return "string";
    }
}
