package clinic.model;

import clinic.to.TimeSlot;
import clinic.util.ProxyGenerator;
import clinic.util.annotations.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.*;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Set;

public class Appointment extends AbstractBaseEntity implements Comparable<Appointment> {
    @NotNull
    private LocalTime from;

    @NotNull
    private Duration duration;

    public Appointment() {
    }

    public static Appointment getInstance(){
        return new ProxyGenerator<Appointment>().getProxy(new Appointment());
    }

    public static Appointment getInstance(LocalTime from, Duration duration){
        return new ProxyGenerator<Appointment>().getProxy(new Appointment(from, duration));
    }

    public static Appointment getInstance(Integer id, LocalTime from, Duration duration){
        return new ProxyGenerator<Appointment>().getProxy(new Appointment(id, from, duration));
    }

    private Appointment(LocalTime from, Duration duration) {
        this(null, from, duration);
    }

    private Appointment(Integer id, LocalTime from, Duration duration) {
        this.id = id;
        this.from = from;
        this.duration = duration;
    }

    public LocalTime getFrom() {
        return from;
    }

    public void setFrom(LocalTime from) {
        this.from = from;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "from=" + from +
                ", duration=" + duration.toString().replace("PT", "") +
                '}';
    }

    @Override
    public int compareTo(Appointment o) {
        return (int) Duration.between(o.getFrom(), getFrom()).toMinutes();
    }

    @Override
    @Validate
    public void validate() {}

    public TimeSlot toTimeSlot(boolean isFree){
        return new TimeSlot(this, isFree);
    }
}
