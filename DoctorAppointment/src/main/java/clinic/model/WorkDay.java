package clinic.model;

import clinic.util.ProxyGenerator;
import clinic.util.annotations.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.ValidationException;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

public class WorkDay extends AbstractBaseEntity{
    Logger log = LogManager.getLogger(WorkDay.class);

    @NotNull
    protected LocalTime start;
    @NotNull
    protected LocalTime end;
    @NotNull
    protected Set<Appointment> appointments;

    {
        Properties prop = new Properties();
        String filename = "clinic.properties";

        try (InputStream input = this.getClass().getClassLoader().getResourceAsStream(filename)) {
            prop.load(input);
            start = LocalTime.parse(prop.getProperty("timeStart"), DateTimeFormatter.ISO_LOCAL_TIME);
            end = LocalTime.parse(prop.getProperty("timeEnd"), DateTimeFormatter.ISO_LOCAL_TIME);
            appointments = new TreeSet<>();

        } catch (IOException e) {
            log.error(e.getMessage());
        }
        validate();
    }

    public static WorkDay instance(){
        return new ProxyGenerator<WorkDay>().getProxy(new WorkDay());
    }

    public Set<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(Set<Appointment> appointments) {
        this.appointments = appointments;
    }

    public LocalTime getStart() {
        return start;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "WorkDay{" +
                "start=" + start +
                ", end=" + end +
                ", appointments=" + appointments +
                '}';
    }

    @Override
    @Validate
    public void validate() {
        if (end.equals(start) || end.isBefore(start)){
            String message = String.format("end must be after start: start = %s, end = %s", start, end);
            LogManager.getLogger(this.getClass()).error(message);
            throw new ValidationException(message);
        }
    }
}
