package clinic.model;

public interface Validatable {
    void validate();
}
