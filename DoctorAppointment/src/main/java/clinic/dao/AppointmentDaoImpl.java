package clinic.dao;

import clinic.model.Appointment;
import clinic.util.DatabaseConnector;
import clinic.util.exceptions.IllegalRequestException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class AppointmentDaoImpl implements AppointmentDao{
    Logger log = LogManager.getLogger(AppointmentDaoImpl.class);
    private DatabaseConnector connector =  new DatabaseConnector();

    @Override
    public Appointment save(Appointment appointment, int workDayId) {
        Time sqlFrom = Time.valueOf(appointment.getFrom());
        int appDuration = (int)appointment.getDuration().toMinutes();

        try {
            PreparedStatement preparedStatement;
            if (appointment.isNew()){
                preparedStatement = connector.getConnection().prepareStatement("INSERT INTO appointments (workday_id, \"from\", duration) VALUES (?, ?, ?)");
                preparedStatement.setInt(1, workDayId);
                preparedStatement.setTime(2, sqlFrom);
                preparedStatement.setInt(3, appDuration);

                int newId = preparedStatement.executeUpdate();
                appointment.setId(newId);
            } else {
                preparedStatement = connector.getConnection().prepareStatement("UPDATE appointments SET \"from\" = ?, duration = ? WHERE id = ?");
                preparedStatement.setTime(1, sqlFrom);
                preparedStatement.setInt(2, appDuration);
                preparedStatement.setInt(3, appointment.getId());

                if (preparedStatement.executeUpdate() == 0){
                    log.error("save appointment: Request is failed for appointment {}", appointment);
                    throw new IllegalRequestException("Request is failed for appointment " + appointment);
                }
            }

            return appointment;
        } catch (SQLException e) {
            log.error("save appointment: Request is failed for appointment {}", appointment);
            return null;
        } finally {
            connector.closeConnection();
        }
    }

    @Override
    public boolean delete(int id) {
        try {
            PreparedStatement preparedStatement = connector.getConnection().prepareStatement("DELETE FROM appointments WHERE appointments.id = ?");
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate() != 0;

        } catch (SQLException e) {
            log.error(e.getMessage());
            return false;
        } finally {
            connector.closeConnection();
        }
    }

    @Override
    public Appointment get(int id) {
        try {
            PreparedStatement preparedStatement = connector.getConnection().prepareStatement("SELECT * FROM appointments WHERE appointments.id = ?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (!resultSet.next()){
                return null;
            }
            Appointment appointment = Appointment.getInstance();
            appointment.setId(resultSet.getInt("id"));
            appointment.setFrom(LocalTime.parse(resultSet.getString("from"), DateTimeFormatter.ISO_LOCAL_TIME));
            appointment.setDuration(Duration.of(resultSet.getInt("duration"), ChronoUnit.MINUTES));


            return appointment;
        } catch (SQLException e) {
            log.error(e.getMessage());
        } finally {
            connector.closeConnection();
        }

        return null;
    }

    @Override
    public List<Appointment> getAll(int workDayId) {
        List<Appointment> list = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connector.getConnection().prepareStatement("SELECT * FROM appointments WHERE workday_id = ?");
            preparedStatement.setInt(1, workDayId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Appointment appointment = Appointment.getInstance();
//                Appointment appointment = new Appointment();
                appointment.setId(resultSet.getInt("id"));
                appointment.setFrom(LocalTime.parse(resultSet.getString("from"), DateTimeFormatter.ISO_LOCAL_TIME));
                appointment.setDuration(Duration.of(resultSet.getInt("duration"), ChronoUnit.MINUTES));
                list.add(appointment);
            }
            return list;
        } catch (SQLException e) {
            log.error(e.getMessage());
        } finally {
            connector.closeConnection();
        }
        return null;
    }
}
