package clinic.to;

import clinic.model.Appointment;
import lombok.Getter;
import lombok.Setter;

import java.time.Duration;
import java.time.LocalTime;

//@Setter
//@Getter
public class TimeSlot implements Comparable<TimeSlot>{
    private final Integer id;
    private LocalTime from;
    private Duration duration;
    private boolean free;

    public TimeSlot(Integer id, LocalTime from, Duration duration, boolean free) {
        this.id = id;
        this.from = from;
        this.duration = duration;
        this.free = free;
    }

    public TimeSlot(Appointment appointment, boolean isFree){
        this(appointment.getId(), appointment.getFrom(), appointment.getDuration(), isFree);
    }

    public Integer getId() {
        return id;
    }

    public LocalTime getFrom() {
        return from;
    }

    public void setFrom(LocalTime from) {
        this.from = from;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public Appointment toAppointment(){
        return Appointment.getInstance(getId(), getFrom(), getDuration());
    }

    @Override
    public int compareTo(TimeSlot o) {
        return (int) Duration.between(o.getFrom(), getFrom()).toMinutes();
    }
}
