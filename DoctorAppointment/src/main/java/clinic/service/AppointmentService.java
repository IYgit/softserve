package clinic.service;

import clinic.model.Appointment;
import clinic.model.WorkDay;
import clinic.util.exceptions.NotFoundException;

import java.util.Collection;
import java.util.List;

public interface AppointmentService {

    List<Appointment> getFreeSlots(WorkDay workDay);

    List<Appointment> getAll(WorkDay workDay);

    Appointment get(int appointmentId) throws NotFoundException;

    Appointment create(Appointment appointment, int workDayId);

    void delete(int id) throws NotFoundException;

    void update(Appointment appointment);
}
