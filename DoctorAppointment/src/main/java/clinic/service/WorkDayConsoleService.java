package clinic.service;

import clinic.model.Appointment;
import clinic.model.WorkDay;
import clinic.util.ProxyGenerator;
import clinic.util.WorkDayUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class WorkDayConsoleService extends WorkDay {
    Logger log = LogManager.getLogger(WorkDayConsoleService.class);

    public WorkDayConsoleService() {
    }

    public static WorkDayConsoleService getInstance(){
        return new ProxyGenerator<WorkDayConsoleService>().getProxy(new WorkDayConsoleService());
    }

    /**
     * Adding new appointment
     *
     * @param appointment - new appointment;
     * @return - true, if appointment is successful added, otherwise - false;
     */
    public boolean addAppointment(Appointment appointment) {
        appointment.validate();
        log.info("addAppointment {}", appointment);
        Set<Appointment> nearestSlots = getNearestSlots(appointment);
        if (checkAppointment(nearestSlots, appointment)) {
            appointments.add(appointment);
            return true;
        }
        return false;
    }

    /**
     * Print nearest slots.
     *
     * @param slots - set of nearest free slots;
     */
    public void printSlots(Set<Appointment> slots) {
        for (Appointment appointment : slots) {
            System.out.println(appointment);
        }
    }

    /**
     * Check whether the appointment is suitable to free slots;
     *
     * @param nearestSlots - sorted nearest free slots;
     * @param appointment  - client appointment;
     * @return - true, if appointment is suitable, otherwise - false;
     */
    public boolean checkAppointment(Set<Appointment> nearestSlots, Appointment appointment) {
        log.info("checkAppointment {}, {}", nearestSlots, appointment);
        Appointment firstSlot = nearestSlots.iterator().next();
        LocalTime to = firstSlot.getFrom().plusMinutes(firstSlot.getDuration().toMinutes());

        return isBetween(appointment, firstSlot.getFrom(), to);
    }

    /**
     * Check whether the client appointment is in range from slotFrom including and
     * slotTo excluding;
     *
     * @param appointment - client desired appointment;
     * @param slotFrom    - start of time slot;
     * @param slotTo      - end of time slot;
     * @return - true, if appointment is in range, otherwise - false;
     */
    public boolean isBetween(Appointment appointment, LocalTime slotFrom, LocalTime slotTo) {
        log.info("isBetween {}, {}, {}", appointment, slotFrom, slotTo);
        LocalTime to = appointment.getFrom().plusMinutes(appointment.getDuration().toMinutes());

        return (appointment.getFrom().equals(slotFrom) || appointment.getFrom().isAfter(slotFrom)) && to.isBefore(slotTo);
    }

    /**
     * Getting nearest to client appointment sorted free slots.
     *
     * @param appointment - client desired appointment;
     * @return - set of nearest free slots;
     */
    public Set<Appointment> getNearestSlots(Appointment appointment) {
        log.info("getNearestSlots {}", appointment);
        return WorkDayUtil.getNearestSlots(getFreeSlots(), appointment);
    }

    /**
     * Getting free slots.
     * @return - set of free slots;
     */
    public Set<Appointment> getFreeSlots() {
        return WorkDayUtil.getFreeSlots(this);
    }

}
