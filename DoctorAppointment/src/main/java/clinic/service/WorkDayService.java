package clinic.service;

import clinic.model.WorkDay;
import clinic.util.exceptions.NotFoundException;

import java.util.List;

public interface WorkDayService {
    List<WorkDay> getAll();

    WorkDay get(int workDayId) throws NotFoundException;

    WorkDay create(WorkDay workDay);

    void delete(int id) throws NotFoundException;

    void update(WorkDay workDay);
}
