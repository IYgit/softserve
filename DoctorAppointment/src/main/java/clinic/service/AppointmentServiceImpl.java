package clinic.service;

import clinic.dao.AppointmentDao;
import clinic.dao.AppointmentDaoImpl;
import clinic.model.Appointment;
import clinic.model.WorkDay;
import clinic.util.WorkDayUtil;
import clinic.util.exceptions.NotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class AppointmentServiceImpl implements AppointmentService {
    private AppointmentDao appointmentDao = new AppointmentDaoImpl();

    @Override
    public List<Appointment> getFreeSlots(WorkDay workDay) {
        workDay.setAppointments(new TreeSet<>(getAll(workDay)));
        return new ArrayList<>(WorkDayUtil.getFreeSlots(workDay));
    }

    @Override
    public List<Appointment> getAll(WorkDay workDay) {
        return appointmentDao.getAll(workDay.getId());
    }

    @Override
    public Appointment get(int appointmentId) throws NotFoundException {
        Appointment appointment = appointmentDao.get(appointmentId);
        if (appointment == null){
            throw new NotFoundException("Not found appointment with id = " + appointmentId);
        }
        return appointment;
    }

    @Override
    public Appointment create(Appointment appointment, int workDayId) {
        return appointmentDao.save(appointment, workDayId);
    }

    @Override
    public void delete(int appointmentId) throws NotFoundException {
        if (!appointmentDao.delete(appointmentId))
            throw new NotFoundException("Not found appointment with id = " + appointmentId);
    }

    @Override
    public void update(Appointment appointment) {
        appointmentDao.save(appointment, -1);
    }
}
