package clinic.service;

import clinic.dao.WorkDayDao;
import clinic.dao.WorkDayDaoImpl;
import clinic.model.WorkDay;
import clinic.util.exceptions.NotFoundException;

import java.util.List;

public class WorkDayServiceImpl implements WorkDayService{
    private WorkDayDao workDayDao = new WorkDayDaoImpl();

    @Override
    public List<WorkDay> getAll() {
        return workDayDao.getAll();
    }

    @Override
    public WorkDay get(int workDayId) throws NotFoundException {
        WorkDay workDay = workDayDao.get(workDayId);
        if (workDay == null){
            throw new NotFoundException("Not found workDay with id = " + workDayId);
        }
        return workDay;
    }

    @Override
    public WorkDay create(WorkDay workDay) {
        return workDayDao.save(workDay);
    }

    @Override
    public void delete(int workDayId) throws NotFoundException {
        if (!workDayDao.delete(workDayId)){
            throw new NotFoundException("Not found workDay with id = " + workDayId);
        }
    }

    @Override
    public void update(WorkDay workDay) {
        workDayDao.save(workDay);
    }
}
