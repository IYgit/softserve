package clinic.util;

import clinic.model.Appointment;
import clinic.model.WorkDay;
import clinic.to.TimeSlot;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Comparator;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Lists.newArrayList;

public class TestDataUtil {
    public static int VALID_APPOINTMENT_ID = 1;
    public static int INVALID_APPOINTMENT_ID = 100;
    public static int VALID_WORKDAY_ID = 1;

    public static TimeSlot NEW_APPOINTMENT = Appointment.getInstance(LocalTime.of(11,0),Duration.of(30,ChronoUnit.MINUTES)).toTimeSlot(false);
    public static WorkDay WORKDAY = getWorkDay();

    public static TimeSlot APPOINTMENT_1 = Appointment.getInstance(LocalTime.of(10,0),Duration.of(60,ChronoUnit.MINUTES)).toTimeSlot(false);
    public static TimeSlot APPOINTMENT_2 = Appointment.getInstance(LocalTime.of(12,0),Duration.of(60,ChronoUnit.MINUTES)).toTimeSlot(false);
    public static TimeSlot APPOINTMENT_3 = Appointment.getInstance(LocalTime.of(15,0),Duration.of(60,ChronoUnit.MINUTES)).toTimeSlot(false);

    public static TimeSlot FREE_SLOT_1 = Appointment.getInstance(LocalTime.of(8,0),Duration.of(120,ChronoUnit.MINUTES)).toTimeSlot(true);
    public static TimeSlot FREE_SLOT_2 = Appointment.getInstance(LocalTime.of(11,0),Duration.of(60,ChronoUnit.MINUTES)).toTimeSlot(true);
    public static TimeSlot FREE_SLOT_3 = Appointment.getInstance(LocalTime.of(13,0),Duration.of(120,ChronoUnit.MINUTES)).toTimeSlot(true);
    public static TimeSlot FREE_SLOT_4 = Appointment.getInstance(LocalTime.of(16,0),Duration.of(60,ChronoUnit.MINUTES)).toTimeSlot(true);

    private static WorkDay getWorkDay() {
        WorkDay workDay = WorkDay.instance();
        workDay.setId(VALID_WORKDAY_ID);
        return workDay;
    }

    public static void assertMatch(TimeSlot actual, TimeSlot expected) {
        Comparator<TimeSlot> comparator = Comparator.comparing(TimeSlot::getFrom)
                .thenComparing(TimeSlot::getDuration)
                .thenComparing(TimeSlot::isFree);
        assertThat(actual).usingComparator(comparator).isEqualTo(expected);
    }

    public static void assertMatch(Iterable<TimeSlot> actual, TimeSlot... expected) {
        Comparator<TimeSlot> comparator = Comparator.comparing(TimeSlot::getFrom)
                .thenComparing(TimeSlot::getDuration)
                .thenComparing(TimeSlot::isFree);
        assertThat(actual).usingElementComparator(comparator).containsExactly(expected);
    }

    public static void assertMatch(Iterable<TimeSlot> actual, Iterable<TimeSlot> expected) {
        assertMatch(actual, expected);
    }

}
