package clinic.util;

import com.ibatis.common.jdbc.ScriptRunner;

import java.io.*;
import java.sql.SQLException;

public class DBScriptRunner {

    public static void executeSqlScript() throws IOException, SQLException {
        InputStream streamInitDb = DBScriptRunner.class.getClassLoader()
                .getResourceAsStream("db/initdb.sql");
        InputStream streamPopulateDb = DBScriptRunner.class.getClassLoader()
                .getResourceAsStream("db/populatedb.sql");
        DatabaseConnector connector = new DatabaseConnector();

        try (Reader readerInitDb = new BufferedReader(new InputStreamReader(streamInitDb));
             Reader readerPopulateDb = new BufferedReader(new InputStreamReader(streamPopulateDb))){
            ScriptRunner scriptExecutor = new ScriptRunner(connector.getConnection(), false, false);
            scriptExecutor.runScript(readerInitDb);
            scriptExecutor.runScript(readerPopulateDb);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connector.closeConnection();
        }
    }
}
