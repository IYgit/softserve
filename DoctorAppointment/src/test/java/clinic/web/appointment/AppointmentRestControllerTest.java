package clinic.web.appointment;

import clinic.to.TimeSlot;
import clinic.util.DBScriptRunner;
import clinic.util.exceptions.NotFoundException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static clinic.util.TestDataUtil.*;

public class AppointmentRestControllerTest {
    private AppointmentController controller = new AppointmentRestController();

    @BeforeMethod
    public void setUp() throws Exception {
        DBScriptRunner.executeSqlScript();
    }

    @Test
    public void testGetFreeSlots(){
        List<TimeSlot> actualFreeSlots = controller.getFreeSlots(WORKDAY);
        assertMatch(actualFreeSlots, FREE_SLOT_1, FREE_SLOT_2, FREE_SLOT_3, FREE_SLOT_4);
    }

    @Test
    public void testGetBusySlots(){
        List<TimeSlot> actualBusySlots = controller.getBusySlots(WORKDAY);
        assertMatch(actualBusySlots, APPOINTMENT_1, APPOINTMENT_2, APPOINTMENT_3);
    }

    @Test
    public void testGet(){
        TimeSlot actual = controller.get(VALID_APPOINTMENT_ID);
        assertMatch(actual, APPOINTMENT_1);
    }

    @Test(expectedExceptions = NotFoundException.class)
    public void testGetNotFound(){
        controller.get(INVALID_APPOINTMENT_ID);
    }

    @Test
    public void testCreate(){
        TimeSlot createdAppointment = controller.create(NEW_APPOINTMENT, VALID_WORKDAY_ID);
        assertMatch(createdAppointment, NEW_APPOINTMENT);
    }

    @Test
    public void testDelete(){
        controller.delete(VALID_APPOINTMENT_ID);
    }

    @Test(expectedExceptions = NotFoundException.class)
    public void testDeleteNotFound(){
        controller.delete(INVALID_APPOINTMENT_ID);
    }

    @Test
    public void testUpdate(){
        TimeSlot appointment = controller.get(VALID_APPOINTMENT_ID);
        appointment.setFrom(NEW_APPOINTMENT.getFrom());
        appointment.setDuration(NEW_APPOINTMENT.getDuration());
        controller.update(appointment);
        assertMatch(controller.get(VALID_APPOINTMENT_ID), NEW_APPOINTMENT);
    }
}