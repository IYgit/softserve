package clinic.service;

import clinic.model.Appointment;
import clinic.util.WorkDayUtil;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.validation.ValidationException;
import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import static clinic.util.WorkDayUtil.*;
import static org.testng.Assert.*;

public class WorkDayConsoleServiceTest {
    private static WorkDayConsoleService wd;
    private final Appointment clientApp = Appointment.getInstance(LocalTime.of(11, 0), Duration.of(30, ChronoUnit.MINUTES));
    private final Appointment clientApp_2 = Appointment.getInstance(LocalTime.of(12, 0), Duration.of(30, ChronoUnit.MINUTES));

    @BeforeMethod
    public static void beforeMethod(){
        wd = WorkDayConsoleService.getInstance();
        Set<Appointment> appointmentSet = wd.getAppointments();
        appointmentSet.add(Appointment.getInstance(LocalTime.of(10, 0), Duration.of(60, ChronoUnit.MINUTES)));
        appointmentSet.add(Appointment.getInstance(LocalTime.of(12, 0), Duration.of(60, ChronoUnit.MINUTES)));
        appointmentSet.add(Appointment.getInstance(LocalTime.of(15, 0), Duration.of(60, ChronoUnit.MINUTES)));
    }

    @Test
    public void successfulAddAppointmentTest() {
        assertTrue(wd.addAppointment(clientApp));
    }

    @Test
    public void failAddAppointmentTest() {
        assertFalse(wd.addAppointment(clientApp_2));
    }

    @Test(expectedExceptions = ValidationException.class)
    public void workDayValidationTest(){
        wd.setEnd(null);
        wd.validate();
    }

    @Test(expectedExceptions = ValidationException.class)
    public void appointmentValidationTest(){
        Appointment appointment = Appointment.getInstance();
        appointment.validate();
    }

    @Test
    public void getFreeSlotsTest() {
        Set<Appointment> expectedSet = new TreeSet<>();
        expectedSet.add(Appointment.getInstance(LocalTime.of(8, 0), Duration.of(2, ChronoUnit.HOURS)));
        expectedSet.add(Appointment.getInstance(LocalTime.of(11, 0), Duration.of(1, ChronoUnit.HOURS)));
        expectedSet.add(Appointment.getInstance(LocalTime.of(13, 0), Duration.of(2, ChronoUnit.HOURS)));
        expectedSet.add(Appointment.getInstance(LocalTime.of(16, 0), Duration.of(1, ChronoUnit.HOURS)));

        assertEquals(wd.getFreeSlots(), expectedSet);
    }

    @Test
    public void getNearestSlotsTest() {
        Set<Appointment> expectedSet = new TreeSet<>(Comparator.comparingLong(
                ap -> Math.abs(Duration.between(ap.getFrom(), clientApp.getFrom()).toMinutes())));
        expectedSet.add(Appointment.getInstance(LocalTime.of(11, 0), Duration.of(1, ChronoUnit.HOURS)));
        expectedSet.add(Appointment.getInstance(LocalTime.of(8, 0), Duration.of(2, ChronoUnit.HOURS)));
        expectedSet.add(Appointment.getInstance(LocalTime.of(13, 0), Duration.of(2, ChronoUnit.HOURS)));
        expectedSet.add(Appointment.getInstance(LocalTime.of(16, 0), Duration.of(1, ChronoUnit.HOURS)));

        assertEquals(wd.getNearestSlots(clientApp), expectedSet);
    }

    @Test
    public void isAppointmentInTimeBoundariesTest() {
        assertTrue(wd.isBetween(clientApp, LocalTime.of(11, 0), LocalTime.of(12, 0)));
        assertTrue(wd.isBetween(clientApp, LocalTime.of(11, 0), LocalTime.of(11, 31)));
    }

    @Test
    public void isNotAppointmentInTimeBoundariesTest() {
        assertFalse(wd.isBetween(clientApp, LocalTime.of(11, 0), LocalTime.of(11, 30)));
        assertFalse(wd.isBetween(clientApp, LocalTime.of(11, 1), LocalTime.of(12, 0)));
    }

    @Test
    public void isAppointmentInNearestFreeSlot() {
        assertTrue(wd.checkAppointment(getNearestSlots(wd.getFreeSlots(), clientApp), clientApp));
    }

    @Test
    public void isNotAppointmentInNearestFreeSlot() {
        assertFalse(wd.checkAppointment(getNearestSlots(wd.getFreeSlots(), clientApp_2), clientApp_2));
    }

}